const request = require('request')

const url = 'http://free-generator.ru/generator.php?action=word&type=0'

module.exports.parseWord = () => {
    return new Promise((res, rej) => {
        request.get(url, {
            headers: {
                'Accept': 'application/json, text/javascript, */*; q=0.01'
            }
        }, (error, response, body) => {
            if (error || response.statusCode !== 200) {
                rej(error)
            }

            res(JSON.parse(body))
        })
    })
}