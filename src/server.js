const {Telegraf} = require('telegraf')
const express = require('express')
const { connectDb } = require('./database/connect')
require('dotenv').config()

const port = Number(process.env.PORT) || 5000
const token = process.env.TOKEN
const dburl = process.env.DB_URL

const bot = new Telegraf(token)
const app = express()

app.get('/', (req, res) => {res.send('Hello, Im bot')})

connectDb(dburl).then(() => {
    app.listen(port, () => console.log(`Bot running on port: ${port}`))
}).catch((e) => {
    console.log(`Bot running with error: ${e}`)
})

module.exports = bot