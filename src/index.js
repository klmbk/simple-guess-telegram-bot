const { Users } = require('./database/schema/user')
const { saveNewUser } = require('./database/scripts/save')
const { startGame, wordCheck, leadControl } = require('./modules/game')
const bot = require('./server')

bot.start(async (ctx) => {
    const chatType = ctx.message.chat.type

    try {
        if (chatType === 'private') {
           return await ctx.reply('Hello, Im a word guessing bot, add me to your group to start the game.')
        }

        const chatid = ctx.message.chat.id
        const userid = ctx.message.from.id
        const username = `[${ctx.message.from.first_name}](tg://user?id=${userid})`
        const messageid = ctx.message.message_id
 
        saveNewUser({
            about: ctx.message.from,
            data: {leading: 0,guessed: 0, rating: 0 }
        })

        startGame(ctx, {chatid, userid, username, messageid})
    } catch (e) {
        console.log(e)
        ctx.reply(e.message )
    }
})

bot.command('mystat', async (ctx) => {
    try {
        const user = await Users.findOne({'about.id' : ctx.message.from.id})

        await ctx.reply(`Имя: ${user.about.first_name}\nВедущий: ${user.data.leading} раз(а)\nОтгадал: ${user.data.guessed} слов`)
    } catch (e) {
        ctx.reply(e.message)
    }
})

bot.command('rating',  async (ctx) => {
    try {
        let result = `Топ-10 игроков\n`
        const users = await Users.find().limit(10) 
    
        users.sort((a, b) => {
            return a.data.guessed - b.data.guessed;
        })
    
        users.reverse()
    
        for (let i = 0; users.length > i; i++) {
            const username = `${users[i].about.first_name} ${users[i].about.last_name ? users[i].about.last_name : ''}`
            result += `\n${i+1}) ${username} - ${users[i].data.guessed} _отгадал(а)_`
        }
    
        await ctx.reply(result, {parse_mode: 'Markdown'})
    } catch (e) {
        ctx.reply(e.message)
    }
} )

bot.on('text', (ctx) => {
    if (ctx.message.chat.type !== 'private') {
        saveNewUser({
            about: ctx.message.from,
            data: {leading: 0,guessed: 0, rating: 0 }
        })
        wordCheck(ctx)
    }
})

bot.on('new_chat_members', async (ctx) => {
    const userid = ctx.message.new_chat_members[0].id
    const username = ctx.message.new_chat_members[0].first_name 
    try {
        const me = await bot.telegram.getMe()

        if (me.id === userid) {
           return await ctx.reply('Hello everyone, Im new. To start a game with me, first give me an admin, then type /start')
        }

        await ctx.reply(`Welcome ${username}!`)
    } catch (e) {
        console.log(e)
        ctx.reply(e.message)
    } 
})

bot.on('callback_query', (ctx) => {
    if (ctx.callbackQuery.message.chat.type !== 'private') {
        leadControl(ctx)
    }
})

bot.launch()