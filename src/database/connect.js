const mongoose = require('mongoose')


module.exports.connectDb = async (url) => {
   try {
     await mongoose.connect(url) 
   } catch (e) {
     throw e
   }
}