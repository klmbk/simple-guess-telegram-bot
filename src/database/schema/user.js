const { model, Schema } = require("mongoose");



module.exports.Users = model('users', new Schema({
    about: {
        id: String,
        first_name: String,
        last_name: String,
        username: String,
        is_bot: String
    },
    data: {
        leading: Number,
        guessed: Number,
        rating: Number,
    },
    date: String
}, {versionKey: false}))