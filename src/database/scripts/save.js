const { Users } = require("../schema/user")

module.exports.saveNewUser = async (user) => {
   try {
       const candidate = await Users.findOne({'about.id' : user.about.id})

       if (candidate) {
           return
       }

       const newUser = new Users({
         about: user.about,
         data: user.data,
         date: new Date().toDateString()
       })

       await newUser.save()
   } catch (e) {
      return
   }
}