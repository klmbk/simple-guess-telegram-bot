const uuid = require('uuid')
const { Users } = require('../database/schema/user')
const { parseWord } = require("./parsing")
 
const games = []
const timeToEnd = 300000

module.exports.startGame = async (ctx, {chatid, userid, username, messageid}) => {
    const game_id = uuid.v4()
    try{
        await ctx.deleteMessage(messageid)
        
        if (checkGame(chatid)) {
            if (ctx.callbackQuery) {
                return await ctx.answerCbQuery('В этом чате игра уже идет!', {show_alert: true})    
            }

            return await ctx.reply('В этом чате игра уже идет!')
        }

        const {message_id} = await ctx.reply(`${username} *обьясняет слово*`, {
            reply_markup: {
                inline_keyboard: [[
                    {
                        text: 'Посмотреть слово',
                        callback_data: 'viewWord'
                    }    
                ], 
               [
                {
                    text: 'Другое слово',
                    callback_data: 'anotherWord'
                }
               ]
            ]
            },
            parse_mode: 'Markdown'
        })

        games.push({
            chatid,
            leading: userid, 
            message_id,
            word: (await parseWord()).word.word.toLowerCase(),
            game_id
        })

        await Users.updateOne({'about.id' : userid}, {$inc: {'data.leading': 1}})

        setTimeout(() => {
            const index = games.findIndex((item) => item.chatid === chatid)
            if (games[index] && games[index].game_id === game_id) {
                ctx.reply(`Игра окончена! Никто не отгадал(а) слово: *${games[index].word}*`, {parse_mode: 'Markdown'})
                games.splice(games[index], 1)
            }
        }, timeToEnd)

    } catch (e) {
        console.log(e)
        ctx.reply(e.message)
    }
}

module.exports.wordCheck = async (ctx) => {
    const text = ctx.message.text.toLowerCase()
    const chatid = ctx.message.chat.id
    const userid = ctx.message.from.id
    const username = `[${ctx.message.from.first_name}](tg://user?id=${userid})`

    try{ 
        const index = games.findIndex((item) => item.chatid === chatid)

        if (!games[index] || games[index].leading === userid) {
            return
        }
    
        if (games[index].word === text && games[index].chatid === chatid) {
            await ctx.reply(`${username} отгадал(а) слово: *${games[index].word}*`, {
                parse_mode: 'Markdown',
                reply_markup: {
                    inline_keyboard: [[
                        {
                            text: 'Хочу быть ведущим!',
                            callback_data: 'restart'
                        }
                    ]]
                }
            })
            await Users.updateOne({'about.id' : userid}, {$inc: {'data.guessed': 1}})
            games.splice(games[index], 1)
        }
    } catch (e) {
        console.log(e)
        ctx.reply(e.message)
    }
}

module.exports.leadControl = async (ctx) => {
   const chatid = ctx.callbackQuery.message.chat.id
   const userid = ctx.callbackQuery.from.id
   const messageid = ctx.callbackQuery.message.message_id
   const username = `[${ctx.callbackQuery.from.first_name}](tg://user?id=${userid})`

   const index = games.findIndex((item) => item.chatid === chatid)

   if (ctx.callbackQuery.data === 'restart') {
      return this.startGame(ctx, {chatid, userid, username, messageid})
   }

   if (!games[index]) {
       return await ctx.answerCbQuery('Эта игра уже окончена!', {show_alert: true})
   }

   if (games[index].leading !== userid) {
       return  await ctx.answerCbQuery(`Это слово не для вас!`, {show_alert: true})
   }

   if (messageid !== games[index].message_id) {
       return await ctx.answerCbQuery('Эта игра уже окончена!', {show_alert: true})
   }

   switch(ctx.callbackQuery.data) {
    case 'viewWord':
        await ctx.answerCbQuery(`Объясните слово: ${games[index].word}`, {show_alert: true})
        break
    case 'anotherWord':
        games[index].word = (await parseWord()).word.word.toLowerCase()
        await ctx.answerCbQuery(`Новое слово: ${games[index].word}`, {show_alert: true})
        break
    default: 
        return
   }
}

const checkGame = (chatid) => {

    for (game of games) {
        if (game.chatid === chatid) {
            return game
        }
    }

    return null
}